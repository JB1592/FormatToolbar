<h1>Description</h1>
The QSI Format Toolbar is a floating toolbar that can be running in the background during development.

It allows the developer to add:
<ul><li>Add Bold, Italic, and Underline to text in Controls, Indicators, and Text
<li>Change the Justification between Left, Center, and Right
<li>Set Text, Foreground, or Background Colors (if available for the object)
<li>Change Font Size
<li>Change Font (supports all installed fonts in the Windows OS)</ul>

The QSI Format Toolbar is launchable in two ways:
<ol><li>Through the Tools Menu at: Tools->QSI Tools->QSI Format Toolbar…
<li>Through a Quick Drop Keyboard Shortcut (QDKS) of Ctrl-1</ol>

<h1>LabVIEW Version</h1>
This code is currently published in LabVIEW 2018

<h1>Build Instructions</h1>
Included in the source is the <b>Format Toolbar.vipb</b> file.  This file is used by the VI Package Manager (VIPM) to build a package that can then be distributed.  Follow the instructions from JKI on how to use VIPM:

<a href="https://vipm.jki.net/">https://vipm.jki.net/</a>

<h1>Installation Guide</h1>
This code is meant to be installed as a VIPM Package.  

Manual installation is not recommended but can be accomplished by putting the right files in the right locations within the LabVIEW folder.

<ul><li> All code in the <b>Main Code</b> folder copy to: <i>[LabVIEW]\vi.lib\QSI\Format Toolbar</i>
<li>The <b>Format Toolbar QD.vi</b> must be copied to: <i>[LabVIEW]\resource\dialog\QuickDrop\plugins</i>
<li>The <b> QSI Format Toolbar.vi</b> must be copied to: <i>[LabVIEW]\project\QSI Tools</i>
<li>The <b> QSI Format Toolbar Help.vi</b> and the <b>The QSI Format Toolbar.pdf</b> must be copied to: <i>[LabVIEW]\help\QSI</i></ul>

After copying the files the following files will need to be opened, re-linked, and saved:
<ul><li><i>[LabVIEW]\project\QSI Tools\QSI Format Toolbar.vi</i> (this VI calls the main Format Toolbar.vi located in the vi.lib folder).
<li><i>[LabVIEW]\resource\dialog\QuickDrop\plugins\Format Toolbar QD.vi</i> (this VI calls the <i>[LabVIEW]\project\QSI Tools\QSI Format Toolbar.vi</i> file)</ul>

<b>CAUTION: After copying avoid linking back to the source.  It is best to uninstall the toolbar while working on the source.  Otherwise cross-linking could occur.</b>

<h1>Execution</h1>
After being launched the QSI Format Toolbar will stay running in the background until it is closed. The developer can carry on programming as normal. 

To apply formatting:
<ol><li>Select the object to be formatted. This can be Selected Text, one or more Controls or Indicators on the Front Panel, or one or more items on the Block Diagram.
<li>Click on the buttons, size, color, or font on the QSI Format Toolbar.
<li>The change should be applied immediately.</ol>

<h1>Support</h1>
Submit Issues or Merge Requests through GitLab.